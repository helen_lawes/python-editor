# Python editor

## Architecture

All the inbound traffic to the website will be directed to the React SPA, this will be part of the server configuration.

The React SPA will load the relevant versioned editor based on the version in the URL. If no version is in the URL then the React SPA will automatically redirect to the latest version.

As part of the React SPA:

* React router dom will be used for routing to the correct editor version
* Each version of the editor will live as its own self contained component

[Configuration diagram](https://photos.app.goo.gl/UmMhMMWiwqgUSPQj9)

## Installation

You'll require the latest version of Node.js. Download the repo and run `npm install`

## Running

`npm start` will start the app in development mode and will start a server usually on http://localhost:3000
