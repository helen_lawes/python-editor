import React from 'react';

const apiContext = React.createContext();

const { Provider, Consumer } = apiContext;

Provider.displayName = 'Api.Provider';
Consumer.displayName = 'Api.Consumer';

export { Provider, Consumer };

export default apiContext;

export class Editor extends React.Component{
	constructor() {
		super();
		this.state = {
			editor: ''
		};
		this.updateEditor = this.updateEditor.bind(this);
	}

	updateEditor(editor) {
		this.setState({
			editor,
		});
	}

	get contextValues() {
		return {
			...this.state,
			updateEditor: this.updateEditor,
		};
	}

	render() {
		let { children } = this.props;
		return (
			<Provider value={this.contextValues}>
				{children}
			</Provider>
		);
	}
}

export const withEditor = Component => {
	const C = React.forwardRef((props, ref) => (
		<Consumer>
			{context => <Component {...props} ref={ref} {...context} />}
		</Consumer>
	));
	C.displayName = `withDoc(${Component.displayName || Component.name})`;
	return C;
};
