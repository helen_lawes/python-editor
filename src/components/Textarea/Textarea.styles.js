import styled from 'styled-components';

const StyledTextarea = styled.textarea`
	width: 500px;
	height: 300px;
	max-width: 100%;
	padding: 10px;
	margin: 0 0 30px;
`;

export default StyledTextarea;
