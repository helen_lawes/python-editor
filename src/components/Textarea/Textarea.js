import React from 'react';

import StyledTextarea from './Textarea.styles';

const Textarea = ({ value }) => (
	<StyledTextarea value={value} />
);

export default Textarea;
