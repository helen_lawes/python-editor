import styled from 'styled-components';

const StyledButton = styled.button`
	padding: 15px 25px;
	border: none;
	background: ${props => props.theme.background};
	border-radius: 4px;
	margin: 0 20px 20px 0;
	font-size: inherit;
`;

export default StyledButton;
