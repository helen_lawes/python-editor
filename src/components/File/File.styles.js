import styled from 'styled-components';

const StyledFile = styled.span`
	display: inline-block;
	input {
		position: absolute;
		left: -9999px;
	}
`;

export default StyledFile;
