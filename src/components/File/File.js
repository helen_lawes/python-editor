import React from 'react';

import StyledButton from '../Button/Button.styles';
import StyledFile from './File.styles';

const Label = StyledButton.withComponent('label');

const fileUpload = onLoaded => {
	return e => {
		const file = e.target.files[0];
		const reader = new FileReader();
		reader.onload = () => {
			onLoaded(reader.result);
		};
		reader.readAsText(file);
	};
}

const File = ({ children, onChange, ...otherProps }) => (
	<StyledFile>
		<input id="file" type="file" onChange={fileUpload(onChange)} {...otherProps} />
		<Label htmlFor="file">{children}</Label>
	</StyledFile>
);

export default File;
