import v1 from './v1';
import v1_1 from './v1_1';
import v2 from './v2';

const Editor = {
	v1,
	v1_1,
	v2,
};

export default Editor;
