import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';

import theme from '../../theme';
import { withEditor } from '../../context/editor';

import Button from '../Button/Button';
import Textarea from '../Textarea/Textarea';
import File from '../File/File';

class Editor extends React.Component {
	showWarning() {
		const text = this.props.editor;
		let routerVersion = this.props.location.pathname.match(/([0-9\.]{1,})/);
		routerVersion = parseInt(routerVersion[0]);
		if (text) {
			let version = text.trim().match(/^([0-9\.]{1,})/);
			if (version) {
				version = parseInt(version, 10);
				if (version !== routerVersion) return true;
			}
		}
		return false;
	}

	render() {
		const { theme, editor } = this.props;
		return (
			<ThemeProvider theme={theme}>
				<React.Fragment>
					{this.showWarning() && <div>Code out of date, view in newer version <Link to="/v/2">2</Link></div>}
					<Textarea value={editor} />
					<div>
						<File onChange={this.props.updateEditor}>Load</File>
						<Button>Save</Button>
					</div>
				</React.Fragment>
			</ThemeProvider>
		);
	}
}

Editor.propTypes = {
	theme: PropTypes.object,
};

Editor.defaultProps = {
	theme: theme.v1_1
};

export default withEditor(Editor);
