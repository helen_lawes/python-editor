import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import { Editor as EditorProvider } from './context/editor';

ReactDOM.render(
	<EditorProvider>
		<App />
	</EditorProvider>
, document.getElementById('root'));
