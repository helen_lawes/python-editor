import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import StyledApp from './App.styles';
import Editor from './components/Editor';

const App = () => (
	<StyledApp>
		<Router>
			<Switch>
				{/* Show editor v 1.1 when viewing /v/1 */}
				<Route exact path="/v/1" render={routerProps => <Editor.v1_1 {...routerProps} />} />
				{/* show editor v 2 when viewing /v/2 */}
				<Route exact path="/v/2" render={routerProps => <Editor.v2 {...routerProps} />} />
				{/* Redirect to latest editor */}
				<Route render={() => <Redirect to="/v/2" />} />
			</Switch>
		</Router>
	</StyledApp>
);

export default App;
