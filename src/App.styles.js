import styled, { injectGlobal } from 'styled-components';

injectGlobal`
	* {
		box-sixing: border-box;
	}

	body {
		font: 16px/normal sans-serif;
	}
`

const StyledApp = styled.div`
	
`;

export default StyledApp;
